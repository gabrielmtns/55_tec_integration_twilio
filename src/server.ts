import 'module-alias/register'
import { processSuccess, processError } from "./service/helpers/process-result"
import { Request } from '@/models/integrator'
import { setAction } from "./service/helpers/actions-mapper"

const express = require('express')
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.post('/', (req, res) => {
  console.log('Messagem recebido', req.body)
  res.send('post')
})

app.post('/sendMessage', async (req, res) => {
  try {
    const response = await setAction('send-message')(req.body)
    res.send(processSuccess('fake_id', response))
    console.log(response)
  } catch (error) {
    console.log(error)
    return res.send(processError('fake_id', error))
  }
 
})

app.listen(3000, () => {
  console.log('Serve works')
})

// function teste() {
//   console.log('teste')
//   tw.messages.create({
//     to: 'whatsapp:+5511952391378',
//     body: 'Teste'
//   }).then((v) => console.log(v)).catch(err => console.log(err))

// }

// teste()
// app.listen(3000, function () {
//   console.log('Server works')
// })

// import 'module-alias/register'