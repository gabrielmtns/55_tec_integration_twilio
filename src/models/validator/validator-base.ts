export interface IValidator {
  isValid: (data: any) => ValidationType
}

export type ValidationType = {
  valid: boolean
  message?: string
}
