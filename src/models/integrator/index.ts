import { Credential } from './credential'

export type Resource = {
  type: string
  [k: string]: any
}

export type Options = {
  to: string
  from?: string
  mediaUrl?: string[]
  message: string
}

type CallbackUrlsType = {
  delivered?: string
  status?: string
  responses?: string
}

export type ProcessResponseData = {
  code?: string
  error?: string
  body?: any
}

export interface ProcessResponse {
  id: string
  response: ProcessResponseData
}

export type Pagination = {
  page: number
  size: number
  total?: number
  nextPage?: number
}
export type Projection = string[]

export type RequestBody = {
  data?: { [field: string]: any }
  credentials?: Credential[]
  pagination?: Pagination
  projection?: Projection
  options?: Options
}

export interface Request {
  id: string
  action: string
  body?: RequestBody
  resources: Resource[]
}

export class RequestError extends Error {
  code: string

  constructor (message: string, code: number | string = '500') {
    super(message)
    this.code = `${code}`
  }
}
