export type Token = string

export type Basic = {
  id: string
  secret: string
}

export type Value = Basic | Token

export type Credential = {
  type: Type
  value: Value
}

export enum Type {
  TOKEN = 'token',
  BASIC = 'basic',
}
