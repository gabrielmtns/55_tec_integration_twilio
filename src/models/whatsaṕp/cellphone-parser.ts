export interface IWhatsappNumberParser {
  setCountryCode(number: string): string
  containsCountryCode(number: string): boolean
  cellphoneFormatIsValid(number: string): boolean
}