import { SendMessageParam, SendMessageResponse } from "./whatsapp-models";

export interface ISendMessage {
  send(param: SendMessageParam): Promise<SendMessageResponse[]> 
}