import { SendMessageParam, SendMessageResponse } from "./whatsapp-models";

export interface IWhatsappService {
  sendMessage(params: SendMessageParam): Promise<SendMessageResponse[]>
}

