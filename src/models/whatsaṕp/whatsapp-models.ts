export type SendMessageParam = {
  from: string, message: string, to: string[], mediaUrl?: string[]
 }
export type SendMessageResponse = {
  account: string,
  body: string,
  errorCode?: string
  errorMessage?: string
  messageId: string
  sid: string
  status: string
}