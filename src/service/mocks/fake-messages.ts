import { MessageInstance } from 'twilio/lib/rest/api/v2010/account/message';
import { SendMessageResponse } from '@/models/whatsaṕp/whatsapp-models';

export const fakeMessageInstance: Partial<MessageInstance> = {
  accountSid: 'any_account_sid',
  body: 'any_body',
  messagingServiceSid: 'any_message_service_id',
  sid: 'any_sid',
  status: 'received',
  errorCode: null,
  errorMessage: null
}

export const fakeMessageResponse: SendMessageResponse = {
  account: fakeMessageInstance.accountSid,
  body: fakeMessageInstance.body,
  messageId: fakeMessageInstance.messagingServiceSid,
  sid: fakeMessageInstance.sid,
  status: fakeMessageInstance.status,
  errorCode: `${fakeMessageInstance.errorCode}`,
  errorMessage: fakeMessageInstance.errorMessage
}

export const fakeIntegratorMessage: any = {
  id: '',
  __ref: {},
  sender_id: '',
  sender___ref: {},
  sender_name: undefined,
  sender_cpf: '',
  sender_cnpj: '',
  sender_companyName: undefined,
  sender_email: '',
  sender_phone: '11944445555',
  sender___type: 'customer',
  receivedBy_0_id: '',
  receivedBy_0___ref: {},
  receivedBy_0_participant_id: '',
  receivedBy_0_participant___ref: {},
  receivedBy_0_participant_name: undefined,
  receivedBy_0_participant_email: '',
  receivedBy_0_participant_phone: '',
  receivedBy_0_participant_branch: '100000',
  receivedBy_0_participant___type: 'user',
  receivedBy_0_startedAt: '2020-09-01T19:00:30.403Z',
  chat_id: '',
  chat___ref: {},
  chat_engager_id: '',
  chat_engager___ref: {},
  chat_engager_participant_id: '',
  chat_engager_participant___ref: {},
  chat_engager_participant_name: undefined,
  chat_engager_participant_email: '',
  chat_engager_participant_phone: '11949545525',
  chat_engager_participant_branch: '100000',
  chat_engager_participant___type: 'user',
  chat_engager_startedAt: '2020-09-01T19:00:30.403Z',
  chat_engaged_0_id: '',
  chat_engaged_0___ref: {},
  chat_engaged_0_participant_id: '',
  chat_engaged_0_participant___ref: {},
  chat_engaged_0_participant_name: undefined,
  chat_engaged_0_participant_cpf: '',
  chat_engaged_0_participant_cnpj: '',
  chat_engaged_0_participant_companyName: undefined,
  chat_engaged_0_participant_email: '',
  chat_engaged_0_participant_phone: '11949545525',
  chat_engaged_1_participant_phone: '11949545525',
  chat_engaged_0_participant___type: 'customer',
  chat_engaged_0_startedAt: '2020-09-01T19:00:30.403Z',
  type: 'text',
  sentAt: '2020-09-01T19:00:30.403Z',
  content: 'Unit test data'
}
