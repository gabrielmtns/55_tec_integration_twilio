import { ValidationType } from '@/models/validator/validator-base'

export const validationError = (message: string): ValidationType => ({
  valid: false, message
})

export const validationOk = (): ValidationType => ({ valid: true })
