import { SendMessageParam } from "@/models/whatsaṕp/whatsapp-models"

export const integratorMessageMapper = (message: { [field: string]: any }, preffix: string, suffix: string): any[] => {
  return Object.values(
    Object.keys(message)
      .filter((key) => key.includes(preffix) && key.includes(suffix))
      .reduce((obj, key) => {
        obj[key] = message[key]
        return obj
      }, {}))
}

export const messageAdapter = (message: { [field: string]: any }): SendMessageParam => {
  return {
    from: null,
    message: message.content,
    to: integratorMessageMapper(message, 'chat_engaged', 'participant_phone'),
  }
}

