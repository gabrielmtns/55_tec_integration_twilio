import { ProcessResponse, ProcessResponseData, RequestError } from '@/models/integrator'

export const processSuccess = (id: string, response: ProcessResponseData): ProcessResponse => (
  {
    id,
    response: { code: '200', body: response }
  }
)

export const processError = (id: string, { code, message }: RequestError): ProcessResponse => (
  {
    id,
    response: { code: `${code || 500}`, error: message }
  }
)
