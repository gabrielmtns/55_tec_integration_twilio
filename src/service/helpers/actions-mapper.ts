import { RequestError } from '../../models/integrator'
import { sendWhatsappMessageFactory } from '../factories/send-message-factory'

const actionsMapper = {
  'send-message': () => {
    const sendMessage = sendWhatsappMessageFactory()
    return sendMessage.send.bind(sendMessage)
  }
}

export function setAction (action: string): any {
  const selectedAction = actionsMapper[action]

  if (!selectedAction) {
    throw new RequestError('Ação invalida')
  }
  return selectedAction()
}
