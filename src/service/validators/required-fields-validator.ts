import { IValidator, ValidationType } from '@/models/validator/validator-base'
import { validationError, validationOk } from '@/service/helpers/validators-response'

export class RequiredFieldsValidator implements IValidator {
  constructor (private readonly fields: string[]) {}
  isValid (data: any): ValidationType {
    if (!data) {
      return validationError('dados não fornecidos')
    }

    const invalidFields = this.fields.filter(f => !data[f])

    if (invalidFields.length) {
      return validationError(`Campo(s) obrigatorio(s): ${invalidFields.join(', ')}`)
    }

    return validationOk()
  }
}
