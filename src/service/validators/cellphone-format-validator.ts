import { IValidator, ValidationType } from '../../models/validator/validator-base'
import { validationError, validationOk } from '../helpers/validators-response'
import { IWhatsappNumberParser } from '@/models/whatsaṕp/cellphone-parser'
import { isArray } from 'util'

export class CellphoneFormatValidator implements IValidator, IWhatsappNumberParser {
  private readonly regexp = /^\+?(?:[0-9]?)\d{8,14}[0-9]$/
  constructor(private readonly fields: string[]) {

  }
  isValid(data: any): ValidationType {
    if (!data) {
      return validationError('Dados não fornecidos')
    }    
    
    const invalidFields = []
    for (const field of this.fields) {
      for (const [index, value] of data[field].entries()) {
        data[field][index] = this.setCountryCode(value)
        this.cellphoneFormatIsValid(value) || invalidFields.push(value)
      }
    }

    if (invalidFields.length) {
      return validationError(`Telefones invalidos: ${invalidFields.join(', ')}`)
    }

    return validationOk()
  }

  cellphoneFormatIsValid(cellphone: string): boolean {
    return this.regexp.test(cellphone)
  }

  containsCountryCode(cellphone: string): boolean {
    return cellphone.startsWith('+')
  }

  setCountryCode(cellphone: string): string {
    if (this.cellphoneFormatIsValid(cellphone) && !this.containsCountryCode(cellphone)) {
      return `+55${cellphone}`
    }
    return cellphone
  }
}