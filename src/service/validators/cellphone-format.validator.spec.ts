import { CellphoneFormatValidator } from './cellphone-format-validator'
import { validationError, validationOk } from '../helpers/validators-response'

const sutFactory = () => {
  return new CellphoneFormatValidator(['field01', 'field02'])
}

describe('Cellphone format validator', () => {
  test('should return a validationError if data isnt provided', () => {
    const sut = sutFactory()
    const result = sut.isValid(null)
    expect(result).toEqual(validationError('Dados não fornecidos'))
  })
  test('should return an error with all invalids numbers if all numbers are invalids', () => {
    const sut = sutFactory()
    const result = sut.isValid({ field01: ['001', '002'], field02: ['002'] })
    expect(result).toEqual(validationError('Telefones invalidos: 001, 002, 002'))
  })
  test('should return an error if any regex not match', () => {
    const sut = sutFactory()
    const result = sut.isValid({ field01: ['11949545525'], field02: ['001'] })
    expect(result).toEqual(validationError('Telefones invalidos: 001'))
  })

  test('should return validationOk if all succeeds', () => {
    const sut = sutFactory()
    const result = sut.isValid({ field01: ['11949545525'], field02: ['+14155552671'] })
    expect(result).toEqual(validationOk())
  })

  describe('Validation - Contains country code', () => {
    test('should return false if number not contains country code', () => {
      const sut = sutFactory()
      const result = sut.containsCountryCode('11949545525')
      expect(result).toBeFalsy()
    })
    test('should return true if number contains country code', () => {
      const sut = sutFactory()
      const result = sut.containsCountryCode('+55949545525')
      expect(result).toBeTruthy()
    })
  })

  describe('Set country code', () => {
    test('should set country code if number not contains', () => {
      const sut = sutFactory()
      const result = sut.setCountryCode('11949545525')
      expect(result).toBe('+5511949545525')
    })
    test('should ignore if + (country code) is provided', () => {
      const sut = sutFactory()
      const result = sut.setCountryCode('+5511949545525')
      expect(result).toBe('+5511949545525')
    })
  });
})