import { RequiredFieldsValidator } from './required-fields-validator'

const sutFactory = (): any => {
  const sut = new RequiredFieldsValidator(['field01', 'field02'])
  return {
    sut
  }
}

describe('Required fields validator', () => {
  test('should return invalid if not data is provided', () => {
    const { sut } = sutFactory()
    const result = sut.isValid()
    expect(result).toEqual({ valid: false, message: 'dados não fornecidos' })
  })

  test('should return invalid if any required field is empty', () => {
    const { sut } = sutFactory()
    const result = sut.isValid({ field01: 'any_value', field02: null })
    expect(result).toEqual({ valid: false, message: 'Campo(s) obrigatorio(s): field02' })
  })

  test('should return valid true if succeeds', () => {
    const { sut } = sutFactory()
    const result = sut.isValid({ field01: 'any_value', field02: 'any_value' })
    expect(result).toEqual({ valid: true })
  })
})
