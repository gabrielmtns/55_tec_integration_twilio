import { IValidator, ValidationType } from '@/models/validator/validator-base'
import { validationError, validationOk } from '@/service/helpers/validators-response'

export class ValidatorComposite implements IValidator {
  constructor (private readonly validators: IValidator[]) { }

  isValid (data: any): ValidationType {
    for (const validator of this.validators) {
      const { valid, message } = validator.isValid(data)
      if (!valid) {
        return validationError(message)
      }
    }
    return validationOk()
  }
}
