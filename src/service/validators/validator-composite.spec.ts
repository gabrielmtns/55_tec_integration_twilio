import { ValidatorComposite } from './validator-composite'
import { IValidator, ValidationType } from '@/models/validator/validator-base'
import { validationError, validationOk } from '../helpers/validators-response'

class ValidatorSpy implements IValidator {
  isValid (data: any): ValidationType {
    return { valid: true }
  }
}

const sutFactory = (): any => {
  const validatorSpy = new ValidatorSpy()
  const sut = new ValidatorComposite([validatorSpy, new ValidatorSpy()])
  return {
    sut,
    validatorSpy
  }
}

describe('Validator Composite', () => {
  test('should return validation error if any validator  is invalid', () => {
    const { sut } = sutFactory()
    jest.spyOn(sut.validators[1], 'isValid').mockReturnValueOnce({ valid: false, message: 'any_message' })
    const result = sut.isValid({ messages: 'any', contacts: ['any'] })
    expect(result).toEqual(validationError('any_message'))
  })
  test('should call only one time each validator', () => {
    const { sut, validatorSpy } = sutFactory()
    const spyValidator1 = jest.spyOn(sut.validators[1], 'isValid')
    const spyValidator2 = jest.spyOn(validatorSpy, 'isValid')
    sut.isValid({ messages: 'any', contacts: ['any'] })
    expect(spyValidator1).toBeCalledTimes(1)
    expect(spyValidator2).toBeCalledTimes(1)
  })
  test('should return validation ok if all validators is valids', () => {
    const { sut } = sutFactory()
    const result = sut.isValid({ messages: 'any', contacts: ['any'] })
    expect(result).toEqual(validationOk())
  })
})
