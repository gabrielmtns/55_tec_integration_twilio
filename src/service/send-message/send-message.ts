import { IValidator } from "@/models/validator/validator-base";
import { SendMessageParam, SendMessageResponse } from "@/models/whatsaṕp/whatsapp-models";
import { ISendMessage } from "@/models/whatsaṕp/send-message";
import { RequestError } from "@/models/integrator";
import { IWhatsappService } from "@/models/whatsaṕp/whatsapp-service";


export class SendMessage implements ISendMessage {
  constructor(
    private readonly validator: IValidator,
    private readonly whatsappService: IWhatsappService
  ) { }

  async send(value: SendMessageParam): Promise<SendMessageResponse[]>{
    const { valid, message } = this.validator.isValid(value)
    if (!valid) {
      throw new RequestError(message, 400)
    }
    return await this.whatsappService.sendMessage(value)
  }

}