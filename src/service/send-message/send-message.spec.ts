import { SendMessage } from './send-message'
import { IValidator, ValidationType } from '../../models/validator/validator-base'
import { IWhatsappService } from '../../models/whatsaṕp/whatsapp-service'
import { SendMessageParam, SendMessageResponse } from '../../models/whatsaṕp/whatsapp-models'
import { RequestError } from '../../models/integrator/index'
import { fakeMessageResponse } from '../mocks/fake-messages'

class ValidatorSpy implements IValidator {
  isValid(): ValidationType {
    return { valid: true }
  }
}

class WhatsappServiceSpy implements IWhatsappService {
  async sendMessage(): Promise<SendMessageResponse[]> {
    return [fakeMessageResponse]
  }
}

const sutFactory = (): any => {
  const validatorSpy = new ValidatorSpy()
  const whatsappServiceSpy = new WhatsappServiceSpy()
  const sut = new SendMessage(validatorSpy, whatsappServiceSpy)
  return {
    sut,
    validatorSpy,
    whatsappServiceSpy
  }
}
const fakeMessageParams: SendMessageParam = {
  from: 'any_from',
  message: 'any_message',
  to: ['any_to']
}
describe('SendMessage', () => {
  test('should call validators with correct params', async () => {
    const { sut, validatorSpy } = sutFactory()
    const spyOn = jest.spyOn(validatorSpy, 'isValid')
    await sut.send(fakeMessageParams)
    expect(spyOn).toHaveBeenCalledWith(fakeMessageParams)
  })
  test('should throw an error if validation fails', async () => {
    const { sut, validatorSpy } = sutFactory()
    jest.spyOn(validatorSpy, 'isValid').mockReturnValueOnce({ valid: false, message: 'any_message' })
    const result = sut.send(fakeMessageParams)
    await expect(result).rejects.toThrowError(new RequestError('any_message', 400))
  })
  test('should call whatsappService sendMessage with correct params', async () => {
    const { sut, whatsappServiceSpy } = sutFactory()
    const spyOn = jest.spyOn(whatsappServiceSpy, 'sendMessage')
    await sut.send(fakeMessageParams)
    expect(spyOn).toHaveBeenCalledWith(fakeMessageParams)
    expect(spyOn).toHaveBeenCalledTimes(1)
  })

  test('should repass throws if sendMessage throws', async () => {
    const { sut, whatsappServiceSpy } = sutFactory()
    jest.spyOn(whatsappServiceSpy, 'sendMessage').mockImplementationOnce(() => {
      throw new Error()
    })
    const result = sut.send(fakeMessageParams)
    await expect(result).rejects.toThrow()
  })

  test('should return a SendMessage if all succeeds', async () => {
    const { sut } = sutFactory()
    const result = await sut.send(fakeMessageParams)
    expect(result).toEqual([fakeMessageResponse])
  })

  
})