import { SendMessage } from "../send-message/send-message";
import { ValidatorComposite } from "../validators/validator-composite";
import { RequiredFieldsValidator } from "../validators/required-fields-validator";
import { CellphoneFormatValidator } from "../validators/cellphone-format-validator";
import { WhatsappService } from "../whatsapp/whatsapp-service";

export const sendWhatsappMessageFactory = (): SendMessage => {
  const validators = new ValidatorComposite([
    new RequiredFieldsValidator(['message', 'to']),
    new CellphoneFormatValidator(['to'])
  ])
  const whatsappService = WhatsappService.instance
  return new SendMessage(validators, whatsappService)
}