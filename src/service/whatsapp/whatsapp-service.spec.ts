import { WhatsappService } from '../whatsapp/whatsapp-service'
import {  fakeMessageInstance } from '../mocks/fake-messages'
import twilio from 'twilio';
import { MessageInstance } from 'twilio/lib/rest/api/v2010/account/message';
import { SendMessageParam } from '../../models/whatsaṕp/whatsapp-models';


jest.mock('twilio', () => {
  return jest.fn(() => {
    return {
      messages: {
        create(): Partial<MessageInstance> {
          return fakeMessageInstance
        }
      }
    }
  })
})

const fakeParams: SendMessageParam = { to: ['any01', 'any02'], from: 'any', message: 'any' }


const sutInstance = (): WhatsappService => {
  return WhatsappService.instance
}

describe('Whatsapp service', () => {
  describe('Singleton', () => {
    test('should set singleton configs when instance is accessed', () => {
      const sut = sutInstance()
      expect(sut).toBeInstanceOf(WhatsappService)
      expect(sut['authToken']).toEqual(expect.any(String))
      expect(sut['accountSid']).toEqual(expect.any(String))
    })
  })
  describe('makeMessage', () => {
    test('should return an array of promises', () => {
      const sut = sutInstance()
      const result = sut['makeMessages'](fakeParams)
      expect(result.length).toBe(fakeParams.to.length)
    })
  });
  describe('Send message', () => {
    test('should call messages create with correct params', async () => {
      const sut = sutInstance()
      const spyOn = jest.spyOn(sut, 'sendMessage')
      await sut.sendMessage(fakeParams)
      expect(spyOn).toHaveBeenCalledWith(fakeParams)
      expect(spyOn).toHaveBeenCalledTimes(1)
    })
    test('should repass error if sendMessage throws', async () => {
      const sut = sutInstance()
      jest.spyOn(sut['client'].messages, 'create').mockImplementationOnce(() => {
        throw new Error()
      })
      const result = sut.sendMessage(fakeParams)
      await expect(result).rejects.toThrow()
    })
    test('should return SendMessageResponse if all succeeds', async () => {
      const sut = sutInstance()
      const result = await sut.sendMessage(fakeParams)
      expect(result).toEqual([{
        account: fakeMessageInstance.accountSid,
        body: fakeMessageInstance.body,
        messageId: fakeMessageInstance.messagingServiceSid,
        sid: fakeMessageInstance.sid,
        status: fakeMessageInstance.status,
        errorCode: fakeMessageInstance.errorCode ? `${fakeMessageInstance.errorCode}`: null,
        errorMessage: fakeMessageInstance.errorMessage
      },
        {
          account: fakeMessageInstance.accountSid,
          body: fakeMessageInstance.body,
          messageId: fakeMessageInstance.messagingServiceSid,
          sid: fakeMessageInstance.sid,
          status: fakeMessageInstance.status,
          errorCode: fakeMessageInstance.errorCode ? `${fakeMessageInstance.errorCode}` : null,
          errorMessage: fakeMessageInstance.errorMessage
        }])
    })
  })
})