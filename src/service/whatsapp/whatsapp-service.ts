import { SendMessageParam, SendMessageResponse } from "@/models/whatsaṕp/whatsapp-models";
import { IWhatsappService } from "@/models/whatsaṕp/whatsapp-service";
import twilio, { Twilio } from 'twilio';
import { env } from "../configs/env";

export class WhatsappService implements IWhatsappService {
  private client: Twilio
  private static _instance: WhatsappService
  private constructor(
    private readonly accountSid: string,
    private readonly authToken: string
  ) {
    this.client = twilio(this.accountSid, this.authToken)
  }
  
 static get instance() {
    if (!WhatsappService._instance) {
      WhatsappService._instance = new WhatsappService(env.TWILIO_ACCOUNT_SID, env.TWILIO_ACCESS_TOKEN)
    }
    return WhatsappService._instance
  }
  async sendMessage(params: SendMessageParam): Promise<SendMessageResponse[]>{
    const response = await Promise.all(this.makeMessages(params))
    return this.responseMapper(response) 
  }

  private makeMessages(params: SendMessageParam): Promise<any>[] {
    const { to, from, message, mediaUrl } = params
    return to.map(t => this.client.messages.create({
      to: `whatsapp:${t}`,
      from: `whatsapp:${from || env.TWILIO_CELLPHONE_NUMBER}`,
      mediaUrl,
      body: message,
    }))
  }
  private responseMapper(response: any[]): any {
    return response.map(r => ({
      account: r.accountSid,
      body: r.body,
      messageId: r.messagingServiceSid,
      sid: r.sid,
      status: r.status,
      errorCode: r.errorCode ? `${r.errorCode}` : null,
      errorMessage: r.errorMessage
    }))
  }
}