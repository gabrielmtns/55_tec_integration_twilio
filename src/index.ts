import 'module-alias/register'
import { Request } from './models/integrator'
import { setAction } from './service/helpers/actions-mapper'
import { messageAdapter } from './service/helpers/message-adapter'
import { processError, processSuccess } from './service/helpers/process-result'

process.on('message', async (request: Request): Promise<any> => {
  const { body: { data }, action, id } = request
  try {
    const response = await setAction(action)(messageAdapter(data))
    return process.send(processSuccess(id, response))
  } catch (error) {
    return process.send(processError(id, error))
  }
})
// async function teste(request: Request): Promise<any> {
//   const { body: { data }, action, id } = request
//   try {
//     const response = await setAction(action)(messageAdapter(data))
//     console.log(response)
//     return process.send(processSuccess(id, response))
//   } catch (error) {
//     console.log(error)
//     return process.send(processError(id, error))
//   }
// }
// teste({
//   action: 'send-message',
//   id: 'any',
//   resources: null,
//   body: {
//     data: fakeIntegratorMessage
//   }
// })